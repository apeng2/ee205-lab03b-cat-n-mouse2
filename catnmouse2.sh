#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo -n "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess: "
read USER_GUESS

while [ "$USER_GUESS" -ne "$THE_NUMBER_IM_THINKING_OF" ]
do

   if [ $USER_GUESS -lt 1 ]
   then
      echo "You must enter a number that's >= 1"
   fi


   if [ $USER_GUESS -gt $THE_MAX_VALUE ]
   then
      echo "You must enter a number that's <= $THE_MAX_VALUE"
   fi


   if [ $USER_GUESS -gt $THE_NUMBER_IM_THINKING_OF ] && [ $USER_GUESS -le $THE_MAX_VALUE ]
   then
      echo "No cat... the number I'm thinking of is smaller than $USER_GUESS"
   fi


   if [ $USER_GUESS -lt $THE_NUMBER_IM_THINKING_OF ] && [ $USER_GUESS -ge 1 ]
   then
      echo "No cat... the number I'm thinking of is larger than $USER_GUESS"
   fi

   echo -n "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess: "
   read USER_GUESS

done

echo "You got me!"
echo " |)---(|  "
echo " ( o o )  "
echo " ==_Y_==  "
echo "   \_/    "


